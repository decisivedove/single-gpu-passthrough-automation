WARNING: THIS IS EXPERIMENTAL AND INCOMPLETE. RUNNING IT MAY RUIN YOUR SYSTEM.
For something more stable, have a look at https://gitlab.com/risingprismtv/single-gpu-passthrough/ .This is just my attempt at doing this in C.
To compile run the `make` command. This shall create 3 executables - 
1. single-gpu-setup
2. vfio-startup
3. vfio-teardown

To remove the 3 compiled executables, simply run `make clean`.

NOTE: This branch is in active development and is not even close to being ready yet.
