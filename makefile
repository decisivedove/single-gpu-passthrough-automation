all: clean
	gcc kernel_parameters.c linuxapilayer.c -o setup_kernel_parameters
	gcc setup_libvirt.c linuxapilayer.c -o setup_libvirt
	gcc startup.c linuxapilayer.c -o vfio-startup
	gcc teardown.c linuxapilayer.c -o vfio-teardown

clean:
	rm -f setup_kernel_parameters
	rm -f setup_libvirt
	rm -f vfio-startup
	rm -f vfio-teardown
